package com.itguru.example;

import com.itguru.example.entity.Book;
import com.itguru.example.entity.Journal;
import com.itguru.example.persistence.HibernateUtil;

import org.hibernate.Session;

public class HibernateTest {
	public static void main(String[] args) {
		Book book = new Book();
        book.setId(1);
        book.setTitle("Evolving Connectionist Systems");
        book.setAuthorName("Kasabov");

        Journal journal = new Journal();
        journal.setId(2);
        journal.setTitle("Computational Intelligence");
        journal.setIssue(5);

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
        session.persist(book);
        session.persist(journal);
 		session.getTransaction().commit();
		session.close();

		HibernateUtil.getSessionFactory().close();
	}
}
