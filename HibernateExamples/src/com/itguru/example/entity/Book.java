package com.itguru.example.entity;

import javax.persistence.Entity;

@Entity
public class Book extends Item {

    private String authorName;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
