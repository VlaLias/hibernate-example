package com.itguru.example.entity;

import javax.persistence.Entity;

@Entity
public class Journal extends Item {

    private int issue;

    public int getIssue() {
        return issue;
    }

    public void setIssue(int issue) {
        this.issue = issue;
    }
}
